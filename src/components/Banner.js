import React from 'react';
import img1 from '../inc/images/banners/banner1.svg';
import img2 from '../inc/images/banners/banner2.svg';
import img3 from '../inc/images/banners/banner3.svg';

function Banner() {
    return(
        <div className="hero">                                                                 
            <div className="container">
                <div id="carouselExampleLight" className="carousel carousel-light slide" data-bs-ride="carousel">
                    <div className="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div className="carousel-inner">
                        <div className="carousel-item active" data-bs-interval="10000">
                            <img src={img1} className="d-block w-100" alt="..." />
                            <div className="carousel-caption d-none d-md-block">
                                <div className="caption">
                                    <h5>Changing the world, 50/50.</h5>
                                    <p className="mb-2 mb-xxl-3">
                                        Procuring a new charity retail system can make a huge difference to organisational processes and colleague experience.
                                    </p>
                                    <button className="btn btn-secondary text-white px-xxl-4 py-xxl-2">Read More</button>
                                </div>
                            </div>
                        </div>
                        <div className="carousel-item" data-bs-interval="2000">
                            <img src={img2} className="d-block w-100" alt="..." />
                            <div className="carousel-caption d-none d-md-block">
                                <div className="caption">
                                    <h5 className="text-white">50/50 x MCG Esports</h5>
                                    <p className="mb-2 mb-xxl-3 text-white">
                                        Procuring a new charity retail system can make a huge difference to organisational processes and colleague experience.
                                    </p>
                                    <button className="btn btn-secondary text-white px-xxl-4 py-xxl-2">Buy Now</button>
                                </div>
                            </div>
                        </div>
                        <div className="carousel-item">
                            <img src={img3} className="d-block w-100" alt="..." />
                            <div className="carousel-caption d-none d-md-block">
                                <div className="caption">
                                    <h5>彩子 by Ayako Creme</h5>
                                    <p className="mb-2 mb-xxl-3">
                                        Procuring a new charity retail system can make a huge difference to organisational processes and colleague experience.
                                    </p>
                                    <button className="btn btn-secondary text-white px-xxl-4 py-xxl-2">Buy Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleLight" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleLight" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>
            </div>                                 
        </div>
    )
}

export default Banner;