import React from 'react';
import {Link} from 'react-router-dom';

function Footer(){
    return(
        <div className="footer">
            <div className="line"></div>
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-xl-3 mb-3 mb-xl-0">
                        <div className="row">
                            <h5 className="fw-bold">Profile</h5>
                            <div className="footer--desc w-75">
                                <span>
                                    Lorem ipsum dolor sit amet, cons ecte adipiscing elit. Nullam sed erat finibus, semper neque ut, pul vinar nulla. 
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-xl-3 mb-3 mb-xl-0">
                        <div className="row">
                            <h5 className="fw-bold">Get to know us</h5>
                            <div>
                                <Link to="/" className="footer--link"><span className="d-block py-1">About</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Blog</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Honours List</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Privacy Policy</span></Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-xl-3 mb-3 mb-xl-0">
                        <div className="row">
                            <h5 className="fw-bold">Service</h5>
                            <div>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Reguler</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Donation</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Auction</span></Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-xl-3 mb-3 mb-xl-0">
                        <div className="row">
                            <h5 className="fw-bold">Follow Us</h5>
                            <div>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Instagram</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Tiktok</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Twitter</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Youtube</span></Link>
                                <Link to="/" className="footer--link"><span className="d-block py-1">Facebook</span></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;