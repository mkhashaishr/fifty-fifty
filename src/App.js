import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import ScrollToTop from "./ScrollToTop";
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Home from './pages/Home';
import Category from './pages/Category';

function App() {
  return (
      <Router>
        <ScrollToTop/>
          <div className="App">
            <Navbar/>
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/category">
                  <Category />
                </Route>
              </Switch>
            <Footer/>
          </div>
      </Router>
  );
}

export default App;
