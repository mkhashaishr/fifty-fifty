import React,{useEffect} from 'react';
import Banner from '../components/Banner';
import CategoryComponent from '../components/CategoryComponent';
import CategoryProduct from '../components/CategoryProduct';    
import CategoryFilter from '../components/CategoryFilter';    


function Category() {
    useEffect(() => {
        document.title = "Fifty-fifty | Digital Download"
      }, [])

    return(
        <>
            <Banner />
            <CategoryComponent />
            <div className="container">
                <div className="category-page my-4">
                    <div className="d-flex">                    
                        <CategoryFilter /> 
                        <CategoryProduct />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Category;