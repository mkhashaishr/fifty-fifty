import React from 'react';
import {Link} from 'react-router-dom';
import img1 from '../inc/images/category/1.png';
import img2 from '../inc/images/category/2.png';
import img3 from '../inc/images/category/3.png';
import img4 from '../inc/images/category/4.png';
import img5 from '../inc/images/category/5.png';
import img6 from '../inc/images/category/6.png';
import img7 from '../inc/images/category/7.png';
import img8 from '../inc/images/category/8.png';

function CategoryComponent(){
    return(
        <div className="container">
            <div className="category">
                <div className="line"></div>
                <div className="row category--wrapper flex-row flex-nowrap flex-xxl-wrap m-2 m-md-0 py-3">
                    <div className="card text-center">
                        <div className="card-body py-2">
                            <Link to="/category" className="stretched-link">
                                <img src={img1} alt="" />
                            </Link>
                            <p className="card-text">Download</p>
                        </div>
                    </div>
                    <div className="card text-center">
                        <div className="card-body py-2">
                            <Link to="/category" className="stretched-link">
                                <img src={img2} alt="" />
                            </Link>
                            <p className="card-text">Fashion</p>
                        </div>
                    </div>
                    <div className="card text-center">
                        <div className="card-body py-2">
                            <Link to="/category" className="stretched-link">
                                <img src={img3} alt="" />
                            </Link>
                            <p className="card-text">Accessories</p>
                        </div>
                    </div>
                    <div className="card text-center">
                        <div className="card-body py-2">
                            <Link to="/category" className="stretched-link">
                                <img src={img4} alt="" />
                            </Link>
                            <p className="card-text">Bags</p>
                        </div>
                    </div>
                    <div className="card text-center">
                        <div className="card-body py-2">
                            <Link to="/category" className="stretched-link">
                                <img src={img5} alt="" />
                            </Link>
                            <p className="card-text">Paint</p>
                        </div>
                    </div>
                    <div className="card text-center">
                        <div className="card-body py-2">
                            <Link to="/category" className="stretched-link">
                                <img src={img6} alt="" />
                            </Link>
                            <p className="card-text">Drinks</p>
                        </div>
                    </div>
                    <div className="card text-center">
                        <div className="card-body py-2">
                            <Link to="/category" className="stretched-link">
                                <img src={img7} alt="" />
                            </Link>
                            <p className="card-text">Watches</p>
                        </div>
                    </div>
                    <div className="card text-center">
                        <div className="card-body py-2">
                            <Link to="/category" className="stretched-link">
                                <img src={img8} alt="" />
                            </Link>
                            <p className="card-text">Foods</p>
                        </div>
                    </div>
                </div>
                <div className="line"></div>
            </div>
        </div>
    )
}

export default CategoryComponent;