import React from 'react';
import img1 from '../inc/images/payment method/1.png';
import img2 from '../inc/images/payment method/2.png';
import img3 from '../inc/images/payment method/3.png';
import img4 from '../inc/images/payment method/4.png';
import img5 from '../inc/images/payment method/5.png';
import img6 from '../inc/images/payment method/6.png';
import img7 from '../inc/images/payment method/7.png';
import img8 from '../inc/images/payment method/8.png';
import img9 from '../inc/images/payment method/9.png';
import img10 from '../inc/images/payment method/10.png';
import img11 from '../inc/images/payment method/11.png';
import img12 from '../inc/images/payment method/12.png';
import img13 from '../inc/images/payment method/13.png';
import img14 from '../inc/images/payment method/14.png';
import img15 from '../inc/images/payment method/15.png';
import img16 from '../inc/images/payment method/16.png';
import img17 from '../inc/images/payment method/17.png';
import img18 from '../inc/images/payment method/18.png';
import img19 from '../inc/images/payment method/19.png';
import img20 from '../inc/images/payment method/20.png';
import img21 from '../inc/images/payment method/21.png';




function PaymentMethod() {
    return(
        <div className="container">
            <div className="payment-method">
                <div className="row">
                    <h5 className="mb-4">Payment Method</h5>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img1} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img2} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img3} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img4} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img5} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img6} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img7} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img8} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img9} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img10} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img11} alt="pm 1" />
                    </div>
                </div>
                <div className="row">
                    <h5 className="mb-4">Bank Transfer</h5>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img12} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img13} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img14} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img15} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img16} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img17} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img18} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img19} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img20} alt="pm 1" />
                    </div>
                    <div className="col-auto mb-3 mb-xxl-0">
                        <img src={img21} alt="pm 1" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PaymentMethod;