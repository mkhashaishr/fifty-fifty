import React from 'react';

function CategoryFilter() {
    const myStyle = {
        width: '14rem',
        height: 'max-content',
        marginRight: '0.2rem',
        border: '1px solid $gray-300',
        borderRadius: '8px'
    }

    return(
        <div className="category-filter">
            <div className="row g-0 d-none d-lg-flex">
                <div className="card" style={myStyle}>
                    <div class="card-header">
                        Filter
                    </div>
                    <div className="row px-2 pt-3 category-filter--search">
                        <label for="customRange2" class="form-label">Search in category</label>
                        <div className="input-group searchbar">
                            <input type="search" className="form-control rounded" placeholder="Search" aria-label="Search"
                              aria-describedby="search-addon" 
                              />
                            <button type="button" className="btn btn-outline-secondary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <hr />
                    <div className="row">
                        <label for="customRange2" class="form-label px-4">Minimum percentage</label>
                        <input type="range" class="form-range px-4" min="0" max="5" id="customRange2"></input>
                        <div className="d-flex gap-4" style={{marginLeft: '0.3rem'}}>
                            <span>50</span>
                            <span>60</span>
                            <span>70</span>
                            <span>80</span>
                            <span>90</span>
                            <span>100</span>
                        </div>
                    </div>
                    <hr/>
                    <div className="price">
                        <label for="flexCheckDefault" class="form-label px-3">Price</label>
                        <div className="row mb-2 px-4">
                            <input type="number" class="form-control" id="exampleFormControlInput1" placeholder="Min Price" />
                        </div>
                        <div className="row mb-2 px-4">
                            <input type="number" class="form-control" id="exampleFormControlInput1" placeholder="Max Price" />
                        </div>
                    </div>
                    <hr />
                    <div className="row px-4">
                        <label for="flexCheckDefault" class="form-label" style={{marginLeft : '-0.7rem'}}>Rating</label>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                            <label class="form-check-label" for="flexCheckDefault">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16" style={{marginTop: '-0.3rem',marginRight: '0.3rem',}}>
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg>
                                 4 Keatas
                            </label>
                        </div>
                    </div>
                    <hr />
                    <div className="row px-4">
                        <label for="flexCheckDefault" class="form-label" style={{marginLeft : '-0.7rem'}}>Pre order</label>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                            <label class="form-check-label" for="flexCheckDefault">
                                Pre order
                            </label>
                        </div>
                    </div>
                    <hr />
                    <div className="row px-4 mb-3">
                        <button className="btn btn-secondary text-white">
                            Apply Filter
                        </button>
                    </div>
                </div>    
            </div>
        </div>
    )
}

export default CategoryFilter;