import React from 'react';
import product1 from '../inc/images/product/home/1.png';
import product2 from '../inc/images/product/home/2.png';
import product3 from '../inc/images/product/home/3.png';
import product4 from '../inc/images/product/home/4.png';
import product5 from '../inc/images/product/home/5.png';
import product6 from '../inc/images/product/home/6.png';
import product7 from '../inc/images/product/home/7.png';
import product8 from '../inc/images/product/home/8.png';
import product9 from '../inc/images/product/home/9.png';
import product10 from '../inc/images/product/home/10.png';
import product11 from '../inc/images/product/home/11.png';
import product12 from '../inc/images/product/home/12.png';
import product13 from '../inc/images/product/home/13.png';
import product14 from '../inc/images/product/home/14.png';
import product15 from '../inc/images/product/home/15.png';
import product16 from '../inc/images/product/home/16.png';
import product17 from '../inc/images/product/home/17.png';
import product18 from '../inc/images/product/home/18.png';
import product19 from '../inc/images/product/home/19.png';
import product20 from '../inc/images/product/home/20.png';
import {Link} from 'react-router-dom';

function ProductFeed() {
    return (
        <div className="container">
            <div className="product-feed my-4">
                <div className="col d-flex product-feed--title">
                    <h5>New Arrivals</h5>
                    <Link to="/category" className="text-link">
                        <span className="mx-1">View All</span>
                    </Link>
                </div>
                <div className="row product-feed--wrapper flex-row flex-nowrap flex-xxl-wrap m-2 m-md-0 py-2">
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product1}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Navy Club Tas Ransel-Tas Laptop Trendy EIBB</p>
                            </Link>
                            <p className="product--price">IDR 300.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product2}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>MATCHA GREEN TEA Powder - FOREST Bubble Drink</p>
                            </Link>
                            <p className="product--price">IDR 35.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product3}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Tas Pria XENDTRIK Tote 01 Bag Genuine leather</p>
                            </Link>
                            <p className="product--price">IDR 135.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product4}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Hoodie Jumper Limitted Editions The Choice Unisex</p>
                            </Link>
                            <p className="product--price">IDR 150.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product5}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>FortKlass Hoodie Lengan Panjang Unisex MVP</p>
                            </Link>
                            <p className="product--price">IDR 225.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product6}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Old Navy Thermal Knit Pullover Henley Hoodie Dark</p>
                            </Link>
                            <p className="product--price">IDR 185.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product7}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Raisa x Darbotz Zipped Hoodie Pullover</p>
                            </Link>
                            <p className="product--price">IDR 345.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product8}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>New States Apparel 5400 Heavyweight T-Shirt</p>
                            </Link>
                            <p className="product--price">IDR 120.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product9}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Tote Bag - anello - TOY TOTE 2WAY Tote Bag - Black</p>
                            </Link>
                            <p className="product--price">IDR 115.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product10}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Teemochi kaos baju Oversized Oversize T-shirt hitam black - S</p>
                            </Link>
                            <p className="product--price">IDR 95.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                </div>
                <div className="line"></div>
                <div className="col d-flex product-feed--title mt-4">
                    <h5>Top-ranked Products</h5>
                    <Link to="/category" className="text-link">
                        <span className="mx-1">View All</span>
                    </Link>
                </div>
                <div className="row product-feed--wrapper flex-row flex-nowrap flex-xxl-wrap m-2 m-md-0 py-2">
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product11}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>T-shirt Vespa Kick Ass / Baju Kaos Distro Pria Wanita Cotton 30s</p>
                            </Link>
                            <p className="product--price">IDR 115.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product12}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>HIBI Ayako (Small Tote) 彩子 - Ayako Creme</p>
                            </Link>
                            <p className="product--price">IDR 75.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product13}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Fear of God Essentials Boxy Photo Series T-Shirt Ivory White - M</p>
                            </Link>
                            <p className="product--price">IDR 150.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product14}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Yoenik Apparel Kikan Shirt Tops Mocca M14378 R1S2</p>
                            </Link>
                            <p className="product--price">IDR 85.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product15}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Etiam consectetur dictum posuere Preset</p>
                            </Link>
                            <p className="product--price">IDR 595.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product16}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>GENERAL F91W1DG / F-91W-1DG / F91W ORIGINAL</p>
                            </Link>
                            <p className="product--price">IDR 800.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product17}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Storage Jar C32/C35 Cereal Kitchen Storage Bottle </p>
                            </Link>
                            <p className="product--price">IDR 125.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product18}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Paint by Numbers Kit DIY - minion by Livramento</p>
                            </Link>
                            <p className="product--price">IDR 200.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product19}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Collected : Contemporary Graphic Design of Indonesia</p>
                            </Link>
                            <p className="product--price">IDR 495.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                    <div className="card product g-0">
                        <Link to="/category">
                            <img src={product20}  alt="Product" className="img-fluid card-img-top" />
                        </Link>
                        <div className="card-body px-2 py-3">
                            <span className="py-1 px-2 rounded product--charity">100%</span>
                            <Link className="product--name" to="/category">
                                <p>Fridayyy P2B-02 Industrial P-Series ORIGINAL</p>
                            </Link>
                            <p className="product--price">IDR 750.000,00</p>
                            <Link to="/category" className="product--seller">
                                <p>Gelang Hrapan</p>
                            </Link>
                            <span className="product--ratings">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg> 5 | Terjual 14
                            </span>
                        </div>
                    </div>
                </div>
                <div className="line"></div>
            </div>
        </div>
    )
}

export default ProductFeed;