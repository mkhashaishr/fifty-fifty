import React from 'react';
import {Link} from 'react-router-dom';
import product1 from '../inc/images/product/category/1.png';
import product2 from '../inc/images/product/category/2.png';
import product3 from '../inc/images/product/category/3.png';
import product4 from '../inc/images/product/category/4.png';
import product5 from '../inc/images/product/category/5.png';
import product6 from '../inc/images/product/category/6.png';
import product7 from '../inc/images/product/category/7.png';
import product8 from '../inc/images/product/category/8.png';

function CategoryProduct(){
    return (
        <>
            <div className="row product-feed--wrapper flex-row flex-nowrap flex-lg-wrap justify-content-start m-2 m-md-0">
                <div className="row g-0 align-items-center mb-1 d-none d-lg-flex">
                    <div className="col-auto">
                        <span className="px-2 category-title">
                            Digital Download
                        </span>
                    </div>
                    <div className="col-auto ms-auto">
                        <div class="btn-group category-sort">
                            <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                Highest Price
                            </button>
                            <ul class="dropdown-menu">
                                <li><Link class="dropdown-item" href="#">Highest Price</Link></li>
                                <li><Link class="dropdown-item" href="#">Lower Price</Link></li>
                                <li><Link class="dropdown-item" href="#">Highest %</Link></li>
                                <li><Link class="dropdown-item" href="#">Lower Price %</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="card product g-0">
                    <Link to="/">
                        <img src={product1}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>Navy Club Tas Ransel-Tas Laptop Trendy EIBB</p>
                        </Link>
                        <p className="product--price">IDR 300.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>
                <div className="card product g-0">
                    <Link to="/">
                        <img src={product2}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>MATCHA GREEN TEA Powder - FOREST Bubble Drink</p>
                        </Link>
                        <p className="product--price">IDR 35.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>
                <div className="card product g-0">
                    <Link to="/">
                        <img src={product3}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>Tas Pria XENDTRIK Tote 01 Bag Genuine leather</p>
                        </Link>
                        <p className="product--price">IDR 135.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>
                <div className="card product g-0">
                    <Link to="/">
                        <img src={product4}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>Hoodie Jumper Limitted Editions The Choice Unisex</p>
                        </Link>
                        <p className="product--price">IDR 150.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>
                <div className="card product g-0">
                    <Link to="/">
                        <img src={product5}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>FortKlass Hoodie Lengan Panjang Unisex MVP</p>
                        </Link>
                        <p className="product--price">IDR 225.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>
                <div className="card product g-0">
                    <Link to="/">
                        <img src={product6}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>Old Navy Thermal Knit Pullover Henley Hoodie Dark</p>
                        </Link>
                        <p className="product--price">IDR 185.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>
                <div className="card product g-0">
                    <Link to="/">
                        <img src={product7}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>Raisa x Darbotz Zipped Hoodie Pullover</p>
                        </Link>
                        <p className="product--price">IDR 345.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>
                <div className="card product g-0">
                    <Link to="/">
                        <img src={product8}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>New States Apparel 5400 Heavyweight T-Shirt</p>
                        </Link>
                        <p className="product--price">IDR 120.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>
                <div className="card product g-0 d-xxl-none">
                    <Link to="/">
                        <img src={product8}  alt="Product" className="img-fluid card-img-top" />
                    </Link>
                    <div className="card-body px-2 py-3">
                        <span className="py-1 px-2 rounded product--charity">100%</span>
                        <Link className="product--name" to="/">
                            <p>New States Apparel 5400 Heavyweight T-Shirt</p>
                        </Link>
                        <p className="product--price">IDR 120.000,00</p>
                        <Link to="/" className="product--seller">
                            <p>Gelang Hrapan</p>
                        </Link>
                        <span className="product--ratings">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#ffc107" className="bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                            </svg> 5 | Terjual 14
                        </span>
                    </div>
                </div>            
            </div>
        </>
    )
}

export default CategoryProduct;