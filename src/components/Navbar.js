import React from 'react';
import {Link} from 'react-router-dom';
import navLogo from '../inc/navLogo.svg';
import Headroom from 'react-headroom';

function Navbar() {
    return(
        <Headroom>
            <nav className="navbar navbar-expand-lg navbar-light bg-white shadow-sm menu">
                <div className="container">
                    <Link className="navbar-brand" to="/">
                        <img src={navLogo} alt="Nav Logo" className="img-fluid" />
                        <span className="mx-1">Fifty-fifty</span></Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link className="nav-link active fw-bold" aria-current="page" to="/">Home</Link>
                            </li>
                            <li className="nav-item dropdown">
                                <Link className="nav-link dropdown-toggle" to="/" id="navbarLightDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Categories
                                </Link>
                                <ul className="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
                                    <li><Link className="dropdown-item" to="/">Download</Link></li>
                                    <li><Link className="dropdown-item" to="/">Fahsion</Link></li>
                                    <li><Link className="dropdown-item" to="/">Accessories</Link></li>
                                    <li><Link className="dropdown-item" to="/">Bags</Link></li>
                                    <li><Link className="dropdown-item" to="/">Paint</Link></li>
                                    <li><Link className="dropdown-item" to="/">Drinks</Link></li>
                                    <li><Link className="dropdown-item" to="/">Wathes</Link></li>
                                    <li><Link className="dropdown-item" to="/">Foods</Link></li>
                                </ul>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Rangkings</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/">About</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Contact</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Cart
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-bag" viewBox="0 0 16 16">
                                        <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z"/>
                                    </svg>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </Headroom>
    )
}

export default Navbar;