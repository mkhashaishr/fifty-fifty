import React,{useEffect} from 'react';
import Banner from '../components/Banner';
import CategoryComponent from '../components/CategoryComponent';
import ProductFeed from '../components/ProductFeed';
import PaymentMethod from '../components/PaymentMethod';


function Home(){
    useEffect(() => {
        document.title = "Fifty-fifty | E Commerce Shops That Donate For A Good Cause"
      }, [])

    return(
        <>
            <Banner />
            <CategoryComponent />
            <ProductFeed />
            <PaymentMethod />
        </>
    )
}

export default Home;